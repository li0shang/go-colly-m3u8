必须文件  templates/*

static/* 

主要是 

[jquery-3.7.0.min.js]: https://code.jquery.com/jquery-3.7.0.j	"a"

## Downloading jQuery using npm or Yarn

```bash
npm install jquery

# yarn add jquery
```

配置文件 必要 config.json

```json
{
    // 保存位置
    "savedir": "/data/动漫下载",
}
```

```apl
前端 -> APK —〉 用websocket 或者 

数据 —〉保存在本地 sqllite 保存数据
两个表格
```

|  id  |   名称    | 集数-[完结/电影] |   封面图-[链接]   | 类型 | 嵌套表 |
| :--: | :-------: | :--------------: | :---------------: | :--: | ------ |
| 默认 | 动漫名称1 |      第8集       | http：//xxxx.png  |  番  | []下表 |
| 默认 | 动漫名称2 |                  | http：//xxxx.jpg  | 电影 | []下表 |
| 默认 | 动漫名称3 |       完结       | http：//xxxx.webp |  番  | []下表 |



|      | 第几集 | 番id |        链         |
| ---- | :----: | :--: | :---------------: |
|      |   1    | 外键 | http：//xxxx.m3u8 |

