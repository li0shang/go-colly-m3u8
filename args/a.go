package args

// PlayerAaa  解析 player_aaaa json返回
type PlayerAaa struct {
	Link     string `json:"link"`      //当前网页链接
	LinkNext string `json:"link_next"` // 下一集链接
	LinkPre  string `json:"link_pre"`  //上一集链接
	Url      string `json:"url"`       // 播放器链接
	UrlNext  string `json:"url_next"`  //下一集mplayer链接 需二次解析
	Note     string `json:"note"`
	Id       string `json:"id"`
}

// M3u8struct  存m3u8相关数据
type M3u8struct struct {
	Title    string // 视频标题 -[ts转mp4 需要用到名字]
	Url      string `json:"url"`
	FilePath string // m3u8 文件保存路径 -[ts转mp4 需要m3u8文件]
	SevaPath string // Ts文件保存目录
	Key      []byte // m3u8 文件解密KEY
	Iv       []byte // m3u8 文件解密 IV b'0000000000000000'
	KeyHost  string // m3u8 文件服务器地址 xxx.xxxxx.com 										// ts m3u8 同地址
	Delpath  string // m3u8 文件ts路径修改为本地 -[需要删除的部分] /xxxx/xxxx/xxxx/  -[xxx.tx]  	// ###
}

// M3u8 M3u8 为了多页面使用
var M3u8 = new(M3u8struct)
