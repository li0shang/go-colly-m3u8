package args

// SearchlistOut  html页面 返回的数据
type SearchlistOut struct {
	List []List `json:"list"`
}

type List struct {
	Name   string `json:"名称"`
	Png    string `json:"封面"`
	Xqurl  string `json:"详情页"`
	Status string `json:"状态"`
	Uptime string `json:"更新时间"`
	Type   string `json:"影片类型"`
}

func (s *SearchlistOut) Add(list List) {
	s.List = append(s.List, list)
}

func (s *SearchlistOut) Clean() {
	s.List = nil
}

// Searchlist 在此声明为了 方便使用
var Searchlist SearchlistOut
