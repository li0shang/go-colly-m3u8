package config

import (
	"fmt"
	"io"
	"os"

	"github.com/pelletier/go-toml/v2"
)

type Config struct {
	Version  string `toml:"version"`
	Server   `toml:"server"`
	Logger   `toml:"logger"`
	Download `toml:"download"`
}

type Download struct {
	DownloadDir string `toml:"downloaddir"`
}
type Logger struct {
	LogLevel int `toml:"loglevel"`
}

type Server struct {
	Port int `toml:"port"`
}

var Conf Config

func init() {
	//打开文件
	file, err := os.Open("config.toml")
	if err != nil {
		panic(err)
		return
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)
	all, err := io.ReadAll(file)
	if err != nil {
		return
	}
	err = toml.Unmarshal(all, &Conf)
	if err != nil {
		panic(err)
		return
	}
	fmt.Printf("\t初始化 WebUi \t%d\n"+
		"\t软件版本\tVersion:%s \n"+
		"\t下载目录->\t%s\n"+
		"\t配置文件->\tconfig.toml\n"+
		"\t日志文件->\tlog/info.log\n", Conf.Port, Conf.Version, Conf.DownloadDir)
}
