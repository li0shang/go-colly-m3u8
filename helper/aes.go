package helper

import (
	"auto/args"
	"crypto/aes"
	"crypto/cipher"
	"fmt"
)

// Decrypt AES-128 解密
func Decrypt(ciphertext, key, iv []byte) ([]byte, error) {
	if iv == nil {
		args.M3u8.Iv = []byte("0000000000000000") // 可有可无
		iv = []byte("0000000000000000")
		fmt.Println("没有iv")
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	plaintext := make([]byte, len(ciphertext))
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(plaintext, ciphertext)
	return plaintext, nil
}
