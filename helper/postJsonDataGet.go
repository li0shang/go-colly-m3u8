package helper

import (
	"encoding/json"

	"github.com/gin-gonic/gin"
)

func PostJsonDataGet(c *gin.Context) any {
	var palyerurl any
	data, _ := c.GetRawData()
	err := json.Unmarshal(data, &palyerurl)
	if err != nil {
		c.JSON(200, gin.H{"err": err, "playerlist": nil})
		return nil
	}
	return palyerurl
}
