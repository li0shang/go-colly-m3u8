package helper

import (
	"auto/args"
	"bytes"
	"fmt"
	"log"
	"os/exec"
)

var m3u8 = args.M3u8

func TstoMP4() {

	fmt.Println("\n \n \n开始执行合并")
	filename := m3u8.FilePath
	Mp42Path := m3u8.SevaPath + m3u8.Title + ".mp4"
	//filename = "物理魔法使马修-06/物理魔法使马修-06.m3"
	//name = "物理魔法使马修-06.mp4"
	fmt.Printf("删除%s目录\n", m3u8.SevaPath)
	del := exec.Command("rm", "-rf", m3u8.SevaPath)
	cmd := exec.Command("ffmpeg", "-i", filename, "-c", "copy", Mp42Path)
	//fmt.Println(filename, name)
	// out用来存储标准输出信息
	var out bytes.Buffer
	cmd.Stdout = &out
	// 执行命令
	err := cmd.Run()
	err = del.Run()
	if err != nil {
		log.Fatal("CMD--->", err)
	}
	// out.String()输出命令执行结果 没有就是最好的
	defer func(errO string) {
		err := recover()
		if err != nil {
			fmt.Println("捕获异常，恢复协程", err)
		}
		fmt.Printf("标准输出信息: %q\n", errO)
	}(out.String())
}
