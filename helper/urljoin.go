package helper

import "net/url"

// UrlJoin 拼接url 传入 两个网址string 返回一个 string
func UrlJoin(a, b string) string {
	host1, _ := url.Parse(a)
	ub, _ := url.Parse(b)
	uo := host1.ResolveReference(ub)
	return uo.String()
}
