package m3

import (
	"errors"
	"fmt"
	u2 "net/url"
	"os"
	"path/filepath"
	"strings"

	"auto/args"
	"auto/config"
	"auto/helper"
	"auto/logger"
)

var M3u8 = args.M3u8
var ParseChan = make(chan string, 1)

func ParseAndDownloadM3U8(title, filename string, urlx string) {
	url := urlx
	logger.Info("开始下载", filename)
here: // Todo 二次解析 跳回来重新解析
	logger.Info("开始解析", url)
	dir := config.Conf.Download.DownloadDir
	body := helper.HttpGet(url)
	//学习filepath包，兼容各操作系统的文件路径
	p1 := filepath.Join(dir, title, filename) // Todo p1 保存ts文件 目录
	M3u8.SevaPath = p1                        // 保存ts文件 目录
	// ~/动漫下载/不知道是啥动漫/第一集
	err := os.MkdirAll(p1, 0777) // 新建目录
	if err != nil {
		fmt.Println("新建文件夹出错 --->", err)
		return
	}
	list := strings.Split(string(body), "\n")
	// 临时保存key -[用来判断是否需要key]
	tmpKeyLine := ""
	for _, s := range list {
		// 这一行文本是否包含 KEY -[是否需要解密]
		if strings.Contains(s, "#EXT-X-KEY") {
			tmpKeyLine = s
		}
		if strings.Contains(s, ".m3u8") {
			x0, _ := u2.Parse(url)
			x1, _ := u2.Parse(s)
			// 拼接 Url
			xok := x0.ResolveReference(x1)
			fmt.Printf("\t需要二次解析")
			M3u8.KeyHost = xok.String()
			url = xok.String()
			goto here // 给新的url  回到上面 重新开始
		}
		// 取开头不是 # 的每行
		if !strings.Contains(s, "#") {
			tmp := strings.Split(s, "/")
			//def 是 地址最后一段
			def := tmp[len(tmp)-1]
			M3u8.Delpath = strings.Replace(s, def, "", -1)
			// 只需要上面的值 -[远程ts路径 转换为本地]
			break
		}
	}

	//fmt.Println("M3u8.Delpath>>>", M3u8.Delpath)
	// 删除多余路径  由于m3u8文件与ts文件在同一目录 -[“”]
	d := strings.Replace(string(body), M3u8.Delpath, "", -1)
	if tmpKeyLine != "" {
		// 删除 Key这一行 -[保存解密后的数据这一行 合并的时候会报错]
		d = strings.Replace(d, tmpKeyLine, "", -1)
	}
	dst := filepath.Join(p1, "index.m3u8")
	M3u8.FilePath = dst
	// 保存处理后的m3u8文件
	err = os.WriteFile(dst, []byte(d), 0666)
	if err != nil {
		fmt.Println(errors.New(fmt.Sprintf("保存文件出错 --->%s", err)))
		return
	}
	// 上面只是修改m3u8文件
	// 解析调用 未修改过得
	JxM3U8(body)
}
