package m3

import (
	"auto/helper"
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

// downloadTS 下载TS文件
func downloadTS(filename string, url string) {
	// 完成一个线程
	defer wg.Done() // goroutine结束
	//return
	var err error
	ciphertext := helper.HttpGet(url)
	// Sok 为ts数据
	var Sok []byte
	if len(M3u8.Key) != 0 {
		// 解密 - [key有值]
		Sok, err = helper.Decrypt(ciphertext, M3u8.Key, M3u8.Iv)
		if err != nil {
			fmt.Printf("解密失败：%s\n", err)
			return
		}
	} else {
		// 不需要解码
		Sok = ciphertext
	}
	//fmt.Printf("解码结果：%s\n", decrypted)
	p := filepath.Join(M3u8.SevaPath, filename)
	fmt.Printf("\r下载%s文件", filename)
	// 数据保存成 Ts文件
	err = os.WriteFile(p, Sok, 0666)
	if err != nil {
		fmt.Println(errors.New(fmt.Sprintf("保存文件出错 %s", err)))
		return
	}
	// 延迟 一秒
	//time.Sleep(1 * time.Second)
	<-GG

}
