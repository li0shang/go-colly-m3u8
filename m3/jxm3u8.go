package m3

import (
	"auto/helper"
	"net/url"
	"regexp"
	"strings"
	"sync"
)

// 添加线程管理层
var wg sync.WaitGroup

// GG 添加线程管理层 -[类似线程池]
var GG = make(chan struct{}, 5)

func JxM3U8(body []byte) {
	//p := (*unsafe.Pointer)(unsafe.Pointer(&c1))
	//c := (*hcchan)(*p)
	//c.closed = 0 //打开被关
	// 将m3u8 网站 -[用来拼接 Key 和 Ts 文件]
	pa, _ := url.Parse(M3u8.KeyHost)
	// 去除收尾空格
	m3f := strings.TrimSpace(string(body))
	// 正则匹配
	reg := regexp.MustCompile(`#EXT-X-KEY:METHOD=.*?,URI=".*?"`)
	// 找Key这行 -[解码需要]
	key := reg.FindString(m3f)
	// 需要解码
	if key != "" {
		reg2 := regexp.MustCompile(`URI=".*?"`)
		keyurl := reg2.FindString(m3f)
		p2, _ := url.Parse(keyurl[5 : len(keyurl)-1])
		keyurlpath := pa.ResolveReference(p2).String()
		keyok := helper.HttpGet(keyurlpath)
		// 将 key 保存
		M3u8.Key = keyok
	}
	// 将 文件按行分割
	list := strings.Split(m3f, "\n")
	for _, j := range list {
		// 这一行不是 # 开头 属于ts文件地址
		if !strings.Contains(j, "#") {
			if !strings.Contains(j, "http") {
				//fmt.Println("需要拼接")
				p1, _ := url.Parse(j)
				x := pa.ResolveReference(p1)
				//fmt.Println("x.String========", x.String())
				go downloadTS(j, x.String())
			} else {
				go downloadTS(strings.Replace(j, M3u8.Delpath, "", -1), j)
			}
			// channel管道 阻塞一下 太快网站会崩
			GG <- struct{}{}
			wg.Add(1)
			//break
		}
	}
	wg.Wait()
	//close(GG)
	// 等待全部下载完毕合并
	helper.TstoMP4()
}
