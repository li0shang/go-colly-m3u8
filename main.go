package main

import (
	_ "auto/config"
	"auto/logger"
	_ "auto/model"
	"auto/view"
)

func main() {
	defer func() {
		err := recover()
		if err != nil {
			//fmt.Printf("程序崩溃原因 %s", err)
			logger.Error("程序崩溃原因 %s", err)
		}
	}()

	// 启动 Gin web界面
	view.InitAPI()
}
