package model

import (
	"fmt"

	//"gorm.io/driver/sqlite"
	"github.com/glebarez/sqlite"
	"gorm.io/gorm"
)

var Md Model
var err error

type Model struct {
	DB *gorm.DB
}

func init() {
	Md.DB, err = gorm.Open(sqlite.Open("dm.db"), &gorm.Config{})
	if err != nil {
		panic("连接数据库失败！")
	}
	err = Md.DB.AutoMigrate(&PlayerListModel{}, &PlayerList{})
	if err != nil {
		fmt.Println(err)
		panic("数据库！")
		return
	}
}
