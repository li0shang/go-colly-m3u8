package model

import (
	"fmt"

	"auto/logger"
	"gorm.io/gorm"
)

type PlayerListModel struct {
	gorm.Model
	Name       string       `json:"name"`
	Img        string       `json:"img" gorm:"column:封面"`
	HomePage   string       `json:"homePage" gorm:"unique"`
	PlayerList []PlayerList `gorm:"foreignKey:IdRefer"`
}

// TableName 表名重写
func (*PlayerListModel) TableName() string {
	return "PlayerList"
}

func (m *PlayerListModel) Add(name, m3url, playurl string) {
	m.PlayerList = append(m.PlayerList, PlayerList{
		Title:     name,
		M3U8Url:   m3url,
		PlayerUrl: playurl,
	})
}

func (m *PlayerListModel) UpLoad2DateBase() any {
	if err = Md.DB.Create(m).Error; err != nil {
		var ind PlayerListModel
		logger.DebugPrint("已有这个数据-->", err.Error())
		err := Md.DB.Where("home_page = ?", m.HomePage).Preload("PlayerList").First(&ind).Error
		if err != nil {
			fmt.Println("新增剧情失败--->", err)
		} else {
			ind.PlayerList = m.PlayerList
			Md.DB.Save(ind)
			return ind
		}
	}
	return *m
}

type PlayerList struct {
	IdRefer   uint
	Title     string `json:"title" gorm:"column:标题"`
	PlayerUrl string `json:"playerUrl" gorm:"column:播放页面;unique"`
	M3U8Url   string `json:"m3u8" gorm:"column:m3u8file"`
}

// TableName 表名重写
func (*PlayerList) TableName() string {
	return "EveryEpisode"
}
