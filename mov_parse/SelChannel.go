package mov_parse

import (
	"auto/args"
	"auto/logger"
)

var (
	Vif V
)

// V 结构体 方便多个同名函数调用
type V interface {
	// Searchlist 搜索列表
	Searchlist(s string) any
	// Getwork 开始解析详情以及播放链接
	Getwork(l args.List) any
	// ParseM3U8 解析m3u8文件
	ParseM3U8(url string) any
}

// SelChannel 选择影视通道
func SelChannel(index int) V {
	logger.DebugPrint("用-->%d号解析通道", index)
	switch index {
	case 1:
		Vif = V1{}
		//default:
		//	logger.DebugPrint("用默认解析通道")
		//	Vif = V1{}
	}
	return Vif
}
