# 

    接口 输入 [影视名称]string
    输出 [播放列表]*PlayerList

``` go// PlayerList  ---------------------------------------------------
// PlayerList  -----
type PlayerList struct {
	Title string `json:"title"` // 搜索的动漫名称
	Png   string                // 动漫封面图网址
	List  []struct {
		Episode string // 第x集名称
		Em3     string // 第x集的m3u8文件地址
	} `json:"list"` // 剧情列表
}
```
