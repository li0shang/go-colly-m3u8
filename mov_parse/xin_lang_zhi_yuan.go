package mov_parse

import (
	"fmt"

	"auto/args"
	"auto/helper"
	"auto/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/imroc/req/v3"
)

// V1 新浪资源采集网 https://xinlangzy.com/
type V1 struct {
}

var (
	api1   = "https://xinlangzy.com/"
	m3u8jx = "https://www.xinlangjiexi.com/m3u8/?url=%s"
	//v1     = collyBase.Clone()
)

func (v V1) Searchlist(s string) any {
	// 清空之前的数据
	args.Searchlist.Clean()
	//
	u := helper.UrlJoin(api1, fmt.Sprintf("%sindex.php/vod/search.html?wd=%s", api1, s))
	// 拼接搜索Url
	fmt.Printf("正在搜索:%s\n", s)
	res, err := req.C().SetUserAgent(ua).R().Get(u)
	if err != nil {
		return nil
	}
	reader, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil
	}
	reader.Find("div.dy-item").Each(func(i int, e *goquery.Selection) {
		at := e.Find("div.dy-name a").Text()          // 标题名称
		au, _ := e.Find("div.dy-name a").Attr("href") // 详情页 还需要 拼接
		stauts := e.Find(".stauts").Text()            // 状态
		uptime := e.Find("div.dy-time > span").Text() // 更新时间
		types := e.Find("div.dy-type > span").Text()  // 影片类型
		urlok := helper.UrlJoin(api1, au)             // 拼接后链接
		args.Searchlist.Add(args.List{
			Name:   at,
			Png:    "",
			Xqurl:  urlok,
			Status: stauts,
			Uptime: uptime,
			Type:   types,
		})
		fmt.Printf("[%v]\t--->\t%-20s  \t\t %s\n", i+1, at, stauts)
	})
	return args.Searchlist
}

func (v V1) Getwork(l args.List) any {
	// 拼接搜索Url
	fmt.Printf("正在获取:%s \n", l.Name)
	res, err := req.C().SetUserAgent(ua).R().Get(l.Xqurl)
	if err != nil {
		return nil
	}
	reader, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil
	}
	var playlist model.PlayerListModel
	img, _ := reader.Find("div.dy-pic>img").Attr("src")
	playlist.Img = img
	reader.Find("div.collect-item").Each(func(i int, e *goquery.Selection) {
		e.Find("div.collect-item-href").Each(func(i int, selection *goquery.Selection) {
			title := selection.Find(".left span").Text()
			herf, _ := selection.Find(".left a").Attr("href")
			playlist.Add(title, herf, fmt.Sprintf(m3u8jx, herf))
		})
		// 多个通道
	})
	return playlist
}

func (v V1) ParseM3U8(url string) any {
	//TODO ---------------
	panic("还没有写----------")
}
