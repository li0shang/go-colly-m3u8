package api

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

func DownloadVideo(c *gin.Context) {
	var err error
	downloadws := websocket.Upgrader{
		ReadBufferSize:   1024,
		WriteBufferSize:  1024,
		CheckOrigin:      func(r *http.Request) bool { return true },
		HandshakeTimeout: time.Second * 30,
	}
	Conn, err := downloadws.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println("不能更新连接:", err)
		return
	}
	defer func(ws *websocket.Conn) {
		err := ws.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(Conn)
	var jsonData = struct {
		Url  string
		Name string
	}{}
	for {
		//读取ws中的数据
		err := Conn.ReadJSON(&jsonData)
		if err != nil {
			//logger.Error("WS读取失败--->", err)
			_ = Conn.WriteJSON(gin.H{"msg": "数据解析失败"})
			return
		}

	}
}
