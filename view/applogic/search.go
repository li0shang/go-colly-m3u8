package applogic

import (
	"auto/args"
	"github.com/gin-gonic/gin"
)

func Search(c *gin.Context) {
	name, _ := c.GetQuery("name")
	defer func() {
		if r := recover(); r != nil {
			c.JSON(200, gin.H{"msg": "解析通道报错请换一个通道｜求片留言", "error": r})
		}
	}()
	data := channel.Searchlist(name).(args.SearchlistOut)
	if len(data.List) == 0 {
		c.JSON(200, gin.H{"msg": "没有找到与[" + name + "]相关的影片", "ok": data})
		return
	}
	c.JSON(200, data.List)
}
