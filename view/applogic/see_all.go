package applogic

import (
	"fmt"

	"auto/model"
	"github.com/gin-gonic/gin"
)

func SeeAll(c *gin.Context) {
	var movlist model.PlayerListModel
	result := model.Md.DB.Preload("PlayerList").Find(&movlist) // 获取全部记录	// SELECT * FROM users;

	if result.RowsAffected > 0 {
		fmt.Println("找到-->", result.RowsAffected)
		c.JSON(200, movlist)
	}

	c.JSON(200, gin.H{"error": result.Error.Error()})
}
