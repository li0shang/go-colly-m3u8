package applogic

import (
	"encoding/json"

	"auto/args"
	"auto/model"
	"github.com/gin-gonic/gin"
)

func Select(c *gin.Context) {
	var jsondata args.List
	data, _ := c.GetRawData()
	err := json.Unmarshal(data, &jsondata)
	if err != nil {
		c.JSON(200, gin.H{"err": err.Error(), "playerlist": nil})
		return
	}
	var pl model.PlayerListModel
	if err = model.Md.DB.Where("home_page = ?", jsondata.Xqurl).Preload("PlayerList").First(&pl).Error; err != nil {
		pl = channel.Getwork(jsondata).(model.PlayerListModel)
		c.JSON(200, gin.H{"err": nil, "playerlist": pl.PlayerList})
		return
	}
	c.JSON(200, gin.H{"err": nil, "playerlist": pl.PlayerList})
}
