package applogic

import (
	"strconv"

	"auto/logger"
	"auto/mov_parse"
	"github.com/gin-gonic/gin"
)

var channel mov_parse.V

func SetChannel(c *gin.Context) {
	a := c.GetHeader("channel")
	if a == "" {
		logger.Error("没有选择解析通道!")
		c.JSON(200, gin.H{"err": "没有选择解析通道"})
		c.Abort()
		return
	} else {
		logger.Info("选择解析通道-->", a)
		atoi, _ := strconv.Atoi(a)
		channel = mov_parse.SelChannel(atoi)
		c.Next()
	}
}
