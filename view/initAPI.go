package view

import (
	"strconv"

	"auto/config"
	apilogic "auto/view/api"
	"auto/view/applogic"
	"github.com/gin-gonic/gin"
)

func InitAPI() {
	g := gin.Default()
	//gin.SetMode(gin.ReleaseMode)
	// websocket APP 手机端交互
	g.GET("/", func(c *gin.Context) {
		c.String(200, "服务正常运行中...")
	})
	g.GET("/wsapp", applogic.WsAppapi)
	//  APi APP 手机端交互
	app := g.Group("/app")
	{
		app.Use(applogic.SetChannel)
		app.GET("/search", applogic.Search)
		app.POST("/select", applogic.Select)
		app.POST("/parse", applogic.Parse)
		app.POST("/download", applogic.DownloadFile)
	}
	api := g.Group("/api")
	{
		api.GET("/seeall", applogic.SeeAll)
		api.GET("/downloadvideo", apilogic.DownloadVideo)
	}

	addr := strconv.Itoa(config.Conf.Server.Port)
	err := g.Run(":" + addr)
	if err != nil {
		return
	}
}
